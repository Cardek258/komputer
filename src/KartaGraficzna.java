/**
 * Created by Robert on 2017-06-08.
 */
public class KartaGraficzna {

    private String name;
    private int size;
    private boolean on;

    public KartaGraficzna(String name, int size){
        this.name = name;
        this.size = size;
    }

    public String showSpecification() {
        return "| Karta graficzna: " + name + " | Rozmiar: " + size;
    }
}

import java.util.Scanner;

/**
 * Created by Robert on 2017-06-08.
 */
public class Komputer {


    private KartaGraficzna nividia = new KartaGraficzna("nividia", 760);
    private Procesor intel = new Procesor("Intel", 2.4);
    private Zasilacz chieftec = new Zasilacz();
    private Zasilacz zalman = new Zasilacz();

    public void run() {
        System.out.println("Komputer uruchamia się");
        System.out.println("| Status zasilacza chieftec: " + chieftec.isOn());
        chieftec.runChieftec();
        System.out.println("| Status zasilacza chieftec: " + chieftec.isOn());
        System.out.println("| Status zasilacza zalman: " + zalman.isOn());
        zalman.runZalman();
        System.out.println("| Status zasilacza zalman: " + zalman.isOn());
        System.out.println("Komputer uruchomił się");
    }

    public void off() {
        System.out.println("Komputer wyłącza się");
        chieftec.offChieftec();
        System.out.println("| Status zasilacza chieftec: " + chieftec.isOn());
        zalman.offZalman();
        System.out.println("| Status zasilacza zalman: " + zalman.isOn());
        System.out.println("Komputer wyłączył się");
    }

    public String showSpecification() {
        return nividia.showSpecification() + "\n" + intel.showSpecification();

    }

    public byte pobieranieObciazenieOdUzytkownika() {
        System.out.println("Podaj procentowe obciążenie procesoraa: ");
        Scanner pobieranie = new Scanner(System.in);
        byte x = pobieranie.nextByte();
        if (x > 70)
            System.out.println("Procesor jest obciazony, komputer wyłączy się automatycznie");
        zalman.offZalman();
        else
            System.out.println("Procesor nie jest obciazony");
        return x;
    }

}







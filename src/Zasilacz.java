/**
 * Created by Robert on 2017-06-08.
 */
public class Zasilacz {

    private boolean on;

    public void runChieftec() {
        on = true;
        System.out.println("Zasilanie Chieftec uruchomione");
    }

    public void offChieftec() {
        on = false;
        System.out.println("Zasilanie Chieftec wyłączone");

    }

    public void runZalman(){
        on = true;
        System.out.println("Zasilanie Zalman uruchomione");
    }
    public void offZalman(){
        on = false;
        System.out.println("Zasilanie Zalman wyłączone");
    }


    public boolean isOn() {
        return on;


    }
}

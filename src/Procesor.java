import java.util.Scanner;

/**
 * Created by Robert on 2017-06-08.
 */
public class Procesor {

    private String name;
    private double predkosc;
    private boolean on;

    public Procesor(String name, double predkosc) {
        this.name = name;
        this.predkosc = predkosc;

    }

    public String getName() {
        return name;
    }

    public double getPredkosc() {
        return predkosc;

    }

    public String showSpecification() {
        return "| Procesor: " + name + " | Prędkość: " + predkosc;

    }


}

